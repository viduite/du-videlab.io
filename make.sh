#!/bin/sh

for arg in "$@"; do
  shift
  case "$arg" in

    "html")
      rm -rf public/*
      cp -r tools/src/index.html tools/lib/
      cp -r tools/lib/* public/
      sed -i '/<div class="content__text" id="vide">/r page.md' public/index.html
    ;;

    *)
      echo ''
      echo 'Vous contemplez le vide.'
      echo ''
      exit 1
    ;;

  esac
done
