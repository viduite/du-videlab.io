Le vide, l’autre nom de cette dissipation vers laquelle tout roman fait signe. Hanté par la disparition, le récit la met en scène, lui invente des personnages, des modalités particulières, des improbables survies.

Un texte, nativement numérique comme celui-ci, se construit dans le réticulaire, dans l’amnésie des flux Internet. L’important n’est pas l’existence des différentes strates, la reprise d’autres textes, le rapprochement de pensées qui maladroitement s’y essaient, mais comment le livre numérique les confie à l’oubli.

Si vous lisez ceci c’est que vous avez parcouru les anamorphoses hypertextuelles, remonter le fil d’une pensée critique qui, par sa forme, voudrait échapper au linéaire. Tout livre signe un point d’étape, une manière virtuelle de passer à autre chose. Une critique au participe présent opterait pour cette dispersion qu’est le numérique. Une pensée perdue dans le flux, dans cette mémoire à l’indexation hasardeuse, telle serait la dissipation réticulaire de l’écriture numérique.

> Le livre est toujours autre, il change et s'échange par la diversité de ses parties, on évite ainsi le mouvement linéaire — le sens unique — de la lecture. De plus, le livre en se déployant et se reployant, se dispersant et se rassemblant, montre qu'il n'a aucune réalité substantielle : il n'est jamais là, sans cesse à se défaire tandis qu'il se fait.

Peut-être, on l’aura compris, ce qui importe est de donner une autre forme au vide, en pirater les fixités, en laisser mouvoir les frontières, tracer des cercles autour d’un objet toujours en altération. Une conception parcellaire : l’écriture numérique serait celle qui s’efface, s’inscrit dans un ailleurs, persiste dans sa fragile absence de support.

Ou plus exactement sur un support qui décline les altérations aléatoires. Imperceptibles changements de propos : espérons qui bruisse le vide cerné ici. Un vide qui serait, bien sûr, la forme première de cette écriture numérique ouverte à la dispersion, car consciente d’être radicalement inachevée, pratiquée dans cette conscience exacerbée de l’immédiateté à laquelle se confronte l’écriture numérique. On essaie ici un livre éloigné de toute forme fixe.


Le vide est un dévoilement repoussé, insupportable. Il apparaît dans ce que l'on tait.  *il faudrait maintenant décrire les intermittences de l'être.* On se garde ici de parler du vide insoutenable du néant, de la mort. Chacun emplira ce livre de ses propres vides ou s'en détournera, trouvera d'autres dédoublements qui, peut-être, en éclaireront les failles. On parle, bien sûr, de ce silence fondamental dans un creux du texte, dans sa virtualité éperdue, égarée au sein d'une version numérique qui se veut - puisque tel est le sujet - sans fin. 
 >  Si l'on pouvait seulement goûter son néant, si l'on pouvait bien se reposer dans son néant, et que ce néant ne soit pas une certaine sorte d'être mais ne soit pas la mort tout à fait.

Les auteurs les plus lucides ont, d'emblée, d'instinct, entrevue leur béance fondatrice. *Je suis cet éternel absent de soi-même.* Le silence des écrivains paraît un mythe encombrant, une de ces résignations que l'on ne saurait admettre. On préfère se pencher sur les discours de substitution qu'ils inventent. 

>Je parle moi de l'absence de trou, d'une sorte de substance froide et sans images, sans sentiment, et qui est comme un heurt indescriptible d'avortements.

Envisageons ce vide avec une certaine réticence, avec le sentiment d'imposture qu'il fait naître en Soi. Personne ne l'a porté avec une acuité comparable à celle d'Antonin Artaud. Peu ont su en bifurer le ratage, comprendre inventer une autre façon de dire cet ineffable vide.
>  Une grande ferveur pensante et surpeuplée portait mon moi comme un abîme plein

Réticence donc à s'approprier ce que l'on ne peut entendre que par emprunts, théoriques qui pis est. À son corps défendant, Artaud est devenu une référence obligatoire. Ai toujours trouvé une obscénité à s'emparer de son discours dans la gratuité d'un confort intellectuel, sans risque d'être si raisonnable. La folie, l'autre vide tacitement tue ici.

> Oui, ma pensée se connaît et elle désespère de s'atteindre. Elle se connaît, je veux dire qu'elle se soupçonne; et en tout cas elle ne se sent plus.

Dans le refus, dans les lettres à Jacques Rivière, mieux que quiconque, avec une intensité avec laquelle on ne sait guère composer, Artaud a exprimé ce vide autour duquel cet essai tourne :  *je suis vacant par stupéfaction de ma langue.* Ensuite c'est la souffrance, celle dont "je" ne saurais parler, la décorporisation, la cochonnerie de la littérature qui approche les rebuts et avortements où Artaud, malgré tout, parvient à exprimer cette douleur que l'on veut taire. Peut-être en a-t-on surtout dit l'heureuse vacance. Artaud nous contraint à comprendre à quel point ce vide, en Soi, est une imposture - emprunts et piratages.  Revenons au roman, joie impersonnelle.  

