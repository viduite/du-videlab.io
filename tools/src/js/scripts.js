// Scripts

// Markdown
let markdownTexts = document.querySelectorAll('.content__text');
markdownTexts.forEach(el => {
  const reader = new commonmark.Parser({smart: true});
  const writer = new commonmark.HtmlRenderer({sourcepos: true, safe: false});
  const parsed = reader.parse(el.textContent);
  el.innerHTML = writer.render(parsed);
});

//
// Scripts
//
// Correct 100 Vh - Source : https://codepen.io/team/css-tricks/pen/WKdJaB
// First we get the viewport height and we multiple it by 1% to get a value for a vh unit
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);

// We listen to the resize event
window.addEventListener('resize', () => {
  // We execute the same script as before
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});


//
// Functions and variables
//
const container = document.querySelector('.content__text');
const paragraphs = document.querySelectorAll('.content__text > p');
const citations = document.querySelectorAll('blockquote');

function shakeEl (el) {
  el.animate([
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' },
    { transform: 'translate(5px,0)' },
    { transform: 'translate(0,0)' }
  ], {
    duration: 1000,
    iterations: 1
  });
}

function isScrolledIntoView(el) {
    const rect = el.getBoundingClientRect();
    const elemTop = rect.top;
    const elemBottom = rect.bottom;

    // Only completely visible elements return true:
    const isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    // Partially into view
    // const isVisible = elemTop < window.innerHeight && elemBottom >= 0;
  //
    // Partially visible elements return true:
    //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
}

function randomNb(min, max) { // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min);
}

function shuffle(array) {
  for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
  }
}

function shuffleParagraphs(container, elements, exceptions) {

  const containerChildren = Array.prototype.slice.call(container.children);
  elements = Array.prototype.slice.call(elements);
  let fixedPos = [];

  // const matchedEls = containerChildren.filter(el => elements.some(check => el == check));

  for (var i = 0; i < containerChildren.length; i++) {
    for (var j = 0; j < exceptions.length; j++) {
      if (containerChildren[i] == exceptions[j]) {
        fixedPos.push(i);
      }
    }
  }

  shuffle(elements);
  let elementsItem = 0;
  for (var i = 0; i < containerChildren.length; i++) {
    if (!fixedPos.includes(i)) {
      container.appendChild(elements[elementsItem]);
      elementsItem++;
    } else {
      container.appendChild(containerChildren[i]);
    }
  }
}

for (paragraph of paragraphs) {
  paragraph.innerHTML += '<div class="border border--top"></div>';
  paragraph.innerHTML += '<div class="border border--bottom"></div>';
}

for (citation of citations) {
  citation.innerHTML += '<div class="border border--top"></div>';
  citation.innerHTML += '<div class="border border--bottom"></div>';
}

//
// Sortable
//
const el = document.querySelector('.content__text');
const sortable = Sortable.create(el,{
  animation: 500,
  handle: '.border',
  // draggable: "p",
  fallbackTolerance: 3,
});

for (let i = 0; i < citations.length; i++) {
  citations[i].setAttribute("id", "citation" + i);
}


//
// Menu
//
const menu = document.querySelector('.menu__items');
const menuBtn = document.querySelector('.btn--menu');
let menuOpen = false;
const info = document.querySelector('.titlepage');
const infoBtn = document.querySelector('.btn--info');
const shuffleParBtn = document.querySelector('.btn--paragraph');
const shuffleCitBtn = document.querySelector('.btn--citation');
const scrollBtn = document.querySelector('.btn--scroll');
const topBtn = document.querySelector('.btn--top');
let scrollShuffle = false;

const debutVide = document.querySelector('#vide');
const formula = document.querySelector('.first');
// Baffle
let baffleTxt = baffle('.baffle')
                .start()
                .set({
                  characters: 'un vide en soi',
                  speed: 150,
                })
                .reveal(2000, 2000);

topBtn.addEventListener('click', (e) => {
  e.preventDefault();
  const scrollVide = new SmoothScroll(debutVide, {
    speed: 1000,
    speedAsDuration: true,
    updateURL: false,
    easing: 'easeInOutCubic'
  });
  scrollVide.animateScroll(debutVide);
  info.classList.add('none');
  topBtn.blur();
});

menuBtn.addEventListener('click', (e) => {
  e.preventDefault();
  let scrollPos = document.body.scrollTop || document.documentElement.scrollTop;
  if (!menuOpen && scrollPos < formula.scrollHeight) {
    const scrollVide = new SmoothScroll(debutVide, {
      speed: 1000,
      speedAsDuration: true,
      updateURL: false,
      easing: 'easeInOutCubic'
    });
    scrollVide.animateScroll(debutVide);
  }
  menu.classList.toggle('none');
  menuOpen = !menuOpen;
  info.classList.add('none');
  menuBtn.blur();
});

infoBtn.addEventListener('click', (e) => {
  e.preventDefault();
  info.classList.toggle('none');
  menuBtn.blur();
});

shuffleParBtn.addEventListener('click', (e) => {
  e.preventDefault();
  shakeEl(container);
  shuffleParagraphs(container, paragraphs, citations)
  info.classList.add('none');
  shuffleParBtn.blur();
});

shuffleCitBtn.addEventListener('click', (e) => {
  e.preventDefault();
  shakeEl(container);
  shuffleParagraphs(container, citations, paragraphs)

  let intoView = false;
  let scrollPos = document.body.scrollTop || document.documentElement.scrollTop;
  let scrollCitPos = [];
  for (let i = 0; i < citations.length; i++) {
    citations[i].setAttribute("id", "citation" + i);
    scrollCitPos.push(Math.abs(citations[i].offsetTop - scrollPos));
    if (isScrolledIntoView(citations[i])) intoView = true;
  }
  if (!intoView) {
    const nextCit = citations[scrollCitPos.indexOf(Math.min(...scrollCitPos))];
    const scrollVide = new SmoothScroll(nextCit, {
      speed: 1000,
      speedAsDuration: true,
      updateURL: false,
      easing: 'easeInOutCubic'
    });
    scrollVide.animateScroll(nextCit);
  }
  info.classList.add('none');
  shuffleCitBtn.blur();
});

scrollBtn.addEventListener('click', (e) => {
  e.preventDefault();
  scrollBtn.classList.toggle('striked');
  scrollShuffle = !scrollShuffle;
  info.classList.add('none');
  scrollBtn.blur();
});

//
// Scroll progress
//
let lastScrollEvent = 0;
let scrollTrigger = 5;
window.addEventListener("scroll", () => {
  if (scrollShuffle) {
    let scrollPos = document.body.scrollTop || document.documentElement.scrollTop;
    let height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    let scrollPercent = Math.round((scrollPos / height) * 100);
    if (scrollPercent >= lastScrollEvent + scrollTrigger || scrollPercent <= lastScrollEvent - scrollTrigger) {
      lastScrollEvent = scrollPercent;
      shakeEl(container);
      shuffleParagraphs(container, paragraphs, citations);
      shuffleParagraphs(container, citations, paragraphs);
    }
  }
});

//
// Smooth Scroll
//
const scroll = new SmoothScroll('a[href*="#"]', {
  speed: 1000,
  speedAsDuration: true,
  easing: 'easeInOutCubic',
  updateURL: false,
});
