// Include Gulp
var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    postcss = require('gulp-postcss'),
    exec = require('child_process').exec;

// CSS
gulp.task('css', function () {
    return gulp.src('tools/src/css/*')
        .pipe(postcss())
        .pipe(gulp.dest('tools/lib/css'));
});

gulp.task('js', function () {
    return gulp.src('tools/src/js/*')
        .pipe(gulp.dest('tools/lib/js'));
});

gulp.task('fonts', function () {
    return gulp.src('tools/src/fonts/*')
        .pipe(gulp.dest('tools/lib/fonts'));
});

gulp.task('img', function () {
    return gulp.src('tools/src/img/*')
        .pipe(gulp.dest('tools/lib/img'));
});

// Browser-sync
gulp.task('browser-sync', function(cb) {
  browserSync({
    server: {
      baseDir: "./public"
    },
    open: false
  }, cb);
});

function reload(done) {
  browserSync.reload();
  done();
}

// Scripts
gulp.task('scripts', function(cb) {
  exec('./make.sh html', cb);
});

gulp.task(
  'remake',
  gulp.series('css', 'js', 'scripts')
);

// Watch
gulp.task('watch', function () {
  gulp.watch([
    'tools/src/css/**/*',
    'tools/src/js/**/*',
  ], gulp.series('remake', reload));
  gulp.watch([
    'make.sh',
    'tools/src/index.html',
    'page.md'
  ], gulp.series('scripts', reload));
});

gulp.task(
  'default',
  gulp.series('css', 'js', 'img', 'fonts', 'scripts', 'browser-sync', 'watch')
);
