# ~/ABRÜPT/MARC VERLYNDE/UN VIDE, EN SOI/PAGE-CRYPTE/*

La *page-crypte* de [l’ouvrage](https://abrupt.cc/marc-verlynde/un-vide-en-soi/) : *Un vide, en Soi*.

## Sur l'antilivre

Par un jeu de collages, emprunts, détournements et liens, *Un vide, en Soi* réfléchit aux représentations du vide inventées dans la littérature contemporaine. Marc Verlynde interroge les inquiétudes qui creusent notre présent par le rapprochement de certains des aspects essentiels du vide croisés dans quelques textes marquants parus ces dernières années.

## Sur l'auteur

Lit souvent, écrit parfois surtout pour s’inventer des viduités.

Son site : [La viduité](https://viduite.wordpress.com/).

## Sur la licence

Cet [antilivre](https://abrupt.cc/antilivre/) est disponible sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Etc.

Vous pouvez également découvrir notre [site](https://abrupt.cc) pour davantage d'informations sur notre démarche, notamment quant au [partage de nos textes](https://abrupt.cc/partage/).
